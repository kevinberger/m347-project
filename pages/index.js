import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>M347 - Kevin Berger</title>
            </Head>
            <main>
                Kevin Berger, M347
            </main>
        </div>
    )
}
